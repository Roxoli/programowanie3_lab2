﻿using System;

namespace P3lab2
{
    class Program
    {

        static void CzyParzysta(int number)
        {
            if (number % 2 == 0)
            {
                Console.WriteLine("Liczba jest parzysta");
            }
            else
            {
                Console.WriteLine("liczba jest nieparzysta");
            }
            


            Console.WriteLine((number % 2 == 0) ? "Parzysta" : "Nieparzysta");
        }
        static void WozekWidlowy()
        {
            int odleglosc = -1, licznik = 0;
            float sredniaodleglosc = 0;

            while (odleglosc != 0)
            {
                Console.WriteLine("Podaj odległość:");
                odleglosc = Convert.ToInt32(Console.ReadLine());
                sredniaodleglosc += odleglosc;
                ++licznik;
            }
            sredniaodleglosc /= --licznik;
            Console.WriteLine("Średnia odległość: {0}", sredniaodleglosc);
        }

        static int[] ID = { 0, 0, 0 };
        static string UnikalnaNazwa(string wybor)
        {
            switch (wybor)
            {
                case "1":
                    ++ID[0];
                    return "Mikser " + ID[0];
                case "2":
                    ++ID[1];
                    return "Sokowirówka " + ID[1];
                case "3":
                    ++ID[2];
                    return "Kombulgulator " + ID[2];
                default:
                    return "noname ";
            }
        }
        static void Main(string[] args)
        {

            string wybor = "";
            string escape = "exit";
            while (!wybor.Equals(escape))
            {
                Console.WriteLine("Podaj nr. produktu (1-3); wyjscie - exit");
                wybor = Console.ReadLine();
                Console.WriteLine("Produkt: {0}", UnikalnaNazwa(wybor));
            }

        }
    }
}